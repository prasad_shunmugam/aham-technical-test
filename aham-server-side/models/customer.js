const Joi = require('joi');

function validateCustomer(newCustomer) {
    const schema = Joi.object({
        firstName: Joi.string().min(3).max(255).required(),
        lastName: Joi.string().min(3).max(255).required(),
        email: Joi.string().min(5).max(255).email().required()
    })
    return schema.validate(newCustomer);
}

exports.validateCustomer = validateCustomer;
