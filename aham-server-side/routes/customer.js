const Joi = require('joi')
const express = require('express');
const router = express.Router();

const { validateCustomer } = require('../models/customer');

// initialized customer database
const customers = [
    {id: 111, firstName: 'Donald', lastName: 'Duck', email: 'donald@gmail.com'},
    {id: 222, firstName: 'Bugs', lastName: 'Bunny', email: 'bugs@gmail.com'},
    {id: 333, firstName: 'tweety', lastName: 'bird', email: 'tweety@gmail.com'},
]


// GET /api/customers retrieve all registered customers
router.get('/', async (req,res) => { 
    return res.send(customers)
})

// GET /api/customers/:id retrieve registered customer
router.get('/:id', async (req,res) => { 
    let targetId = req.params.id

    for (customer in customers) {   
        if (customers[customer].id == targetId)
            return res.send(customers[customer])
      }
      return res.send({"error":"customer does not exist"})    
})

// POST /api/customers register new customer
router.post('/', async (req,res) => { 
    // validate new customer
    const {error} = validateCustomer(req.body)
    if (error) return res.status(400).send(error.message)
    
    // check if email already exist
    for (customer in customers) {   
        if (customers[customer].email == req.body.email) {
            return res.status(400).send({"error":"Someone's already using that email"})
        }
    }    

    // create user ..
    const timestamp = Date.now();

    newCustomer = {
        id: timestamp, 
        firstName: req.body.firstName, 
        lastName: req.body.lastName, 
        email: req.body.email
    }

    customers.push(newCustomer)

    return res.send(newCustomer)    
})

// PUT /api/customers/:id update registered customer
router.put('/:id', async (req,res) => { 
    // validate updated customer
    const {error} = validateCustomer(req.body)
    if (error) return res.status(400).send(error.message)

    // update user
    let targetId = req.params.id

    for (customer in customers) {   
        if (customers[customer].id == targetId) {
            customers[customer].firstName = req.body.firstName
            customers[customer].lastName  = req.body.lastName
            customers[customer].email     = req.body.email
            return res.send(customers[customer])
        }
    }
    return res.send({"error":"customer does not exist"})  
})

// DELETE /api/customers/:id delete registered customer
router.delete('/:id', async (req,res) => { 
    // delete customer
    let targetId = req.params.id

    for (customer in customers) {   
        if (customers[customer].id == targetId) {
            deletedCutomer = customers[customer]
            customers.splice(customer, 1);
            return res.send(deletedCutomer)
        }
    }
    return res.send({"error":"customer does not exist"})    
})

module.exports = router;
