const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');

const customer = require('./routes/customer');


// Configure CORS, invalid cross site   
var cors = require('cors');
const corsOpts = {
    origin: '*',

    methods: [
        'GET',
        'POST',
        'PUT',
        'DELETE'
    ],

    allowedHeaders: [
        'Content-Type',
    ],
};
app.use(cors(corsOpts));


app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))

// routes
app.use('/api/customers', customer);
app.get('/api', (req, res) => { 
  res.send("GET Request Called Load Balancer Connected to API") 
}) 


const port = process.env.PORT || 4000;
// const server = https.createServer(options).listen(port, () => console.log(`Listening HTTPS on port ${port}...`));
const server = app.listen(port, () => console.log(`Listening HTTP on port ${port}...`));

module.exports = server
