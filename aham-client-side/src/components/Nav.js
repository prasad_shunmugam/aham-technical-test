import React from "react";
import { Link } from "react-router-dom";
import logo from '../logo.svg';

class Nav extends React.Component {
  render() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
            <Link className="navbar-brand" to='/'>
                <img src={logo} alt="" width="35" height="29"/>  
            </Link>            
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
            <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to='/'>
                        Home
                    </Link>
                </li>                
                <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to='/register_customer'>
                        New Customer
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to='/existing_customer'>
                        Customer List
                    </Link>
                </li>
            </ul>
            </div>
        </div>
        </nav>
    );
  }
}

export default Nav;
