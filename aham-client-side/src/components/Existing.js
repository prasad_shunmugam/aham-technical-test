import React from "react";
import { Link } from "react-router-dom";

class Existing extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = { 
            customers: [{
                id: 'loading...',
                firstName: "loading...",
                lastName: "loading...",
                email: "loading..."
            }]
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:4000/api/customers';
        const response = await fetch(url);
        const data = await response.json();
        this.setState({customers: data});
    }

    render() {
        const listItems = this.state.customers.map(customer => (
            <tr key={customer.id}>
                <th scope="row">{customer.id}</th>
                <td>{customer.firstName}</td>
                <td>{customer.lastName}</td>
                <td>{customer.email}</td>
                <td>
                    <Link to={`/customer_profile/${customer.id}`}>
                        <button type="button" className="btn btn-primary">
                       View
                        </button>
                    </Link>                    
                </td>
            </tr> 
            )
        );

        console.log(listItems)
        return (
            <div className="container">
                <h1 className="mb-3">Customer List</h1>
                <table className="table">
                <thead>
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {listItems}
                </tbody>
                </table>
            </div>
        );
    }
}

export default Existing;
