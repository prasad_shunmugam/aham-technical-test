import React from "react";
import { Link } from "react-router-dom";

class Home extends React.Component {
  render() {
    return (
      <div className="container">        
        <div>
          <h1 className="display-3 text-center mt-5">Affin Hwang Asset Management Customer Registration</h1>
        </div>
        <div className='d-flex justify-content-around mt-5'>
          <Link to='/register_customer'>
            <button type="button" className="btn btn-primary btn-lg">
              Register New Customer
            </button>
          </Link>
          <Link to='/existing_customer'>
            <button type="button" className="btn btn-success btn-lg">
              View Registered Customers
            </button>
          </Link>

        </div>
      </div>
    );
  }
}

export default Home;
