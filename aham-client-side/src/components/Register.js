import React from "react";
import "./Register.css";

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customer: {
        id: null,
        firstName: "",
        lastName: "",
        email: "",
      },
      isUpdated: false,
      firstNameError: "",
      lastNameError: "",
      emailError: "",
      isError: true,
      requestError: "",
    };

    this.handleChangeFirstName = this.handleChangeFirstName.bind(this);
    this.handleChangeLastName = this.handleChangeLastName.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const { id } = this.props.match.params;
    if (id != null) {
      const url = `http://localhost:4000/api/customers/${id}`;
      const response = await fetch(url);
      const data = await response.json();
      this.setState({ customer: data, isUpdated: true, isError: false });
      this.firstName = this.state.customer.firstName;

      console.log("current state: ", this.state);
    }
  }

  handleChangeFirstName(event) {
    let { customer } = this.state;
    customer.firstName = event.target.value;
    this.setState({ customer });
    this.handleValidation();
  }
  handleChangeLastName(event) {
    let { customer } = this.state;
    customer.lastName = event.target.value;
    this.setState({ customer });
    this.handleValidation();
  }
  handleChangeEmail(event) {
    let { customer } = this.state;
    customer.email = event.target.value;
    this.setState({ customer });
    this.handleValidation();
  }

  handleValidation() {
    let firstNameError = "";
    let lastNameError = "";
    let emailError = "";
    let isError = true;

    if (this.state.customer.firstName === "") {
      firstNameError = "Please provide a valid first name";
    } else if (
      this.state.customer.firstName.length > 255 ||
      this.state.customer.firstName.length < 3
    ) {
      firstNameError = "first name has to be between 3 to 255 characters";
    }

    if (this.state.customer.lastName === "") {
      lastNameError = "Please provide a valid last name";
    } else if (
      this.state.customer.lastName.length > 255 ||
      this.state.customer.lastName.length < 3
    ) {
      lastNameError = "last name has to be between 3 to 255 characters";
    }

    if (this.state.customer.email === "") {
      emailError = "Please provide a valid email";
    } else if (!this.state.customer.email.includes("@")) {
      emailError = "Please provide a valid email";
    } else if (
      this.state.customer.email.length > 255 ||
      this.state.customer.email.length < 3
    ) {
      emailError = "email has to be between 3 to 255 characters";
    }

    if (firstNameError + lastNameError + emailError === "") isError = false;

    this.setState({ firstNameError, lastNameError, emailError, isError });
  }

  handleSubmit(event) {
    event.preventDefault();
    const id = this.state.customer.id;
    if (this.state.isUpdated) {
      this.handleRequest("PUT", id);
    } else {
      this.handleRequest("POST", id);
    }
  }

  handleRequest(requestType, id = null) {
    let url = `http://localhost:4000/api/customers`;
    if (this.state.isUpdated) {
      url = `http://localhost:4000/api/customers/${id}`;
    }

    let requestOptions = {
      method: requestType,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        firstName: this.state.customer.firstName,
        lastName: this.state.customer.lastName,
        email: this.state.customer.email,
      }),
    };

    fetch(url, requestOptions)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        if (this.state.isUpdated) {
          console.log("Updated user: ", data);
          this.setState({ requestError: "" });
        } else {
          console.log("new registered user: ", data);
          this.setState({ requestError: "" });
        }

        if (data.error === undefined) {
          window.location.href = "/existing_customer";
        } else {
          this.setState({ requestError: data.error });
        }
      });
  }

  render() {
    return (
      <div className="container">
        <div>
          {this.state.isUpdated ? (
            <h1>Update Customer Registration</h1>
          ) : (
            <h1>New Customer Registration</h1>
          )}
        </div>
        <form>
          <div className="mb-3">
            <label htmlFor="firstName" className="form-label">
              First Name
            </label>
            <input
              type="text"
              className="form-control"
              id="firstName"
              aria-describedby="firstNameHelp"
              value={this.state.customer.firstName}
              onChange={this.handleChangeFirstName}
            />
            <div className="invalid-feedback-def">
              {this.state.firstNameError}
            </div>
          </div>
          <div className="mb-3">
            <label htmlFor="lastName" className="form-label">
              Last Name
            </label>
            <input
              type="text"
              className="form-control"
              id="lastName"
              aria-describedby="lastNameHelp"
              value={this.state.customer.lastName}
              onChange={this.handleChangeLastName}
            />
            <div className="invalid-feedback-def">
              {this.state.lastNameError}
            </div>
          </div>
          <div className="mb-3">
            <label htmlFor="email" className="form-label">
              Email address
            </label>
            <input
              type="email"
              className="form-control"
              id="email"
              aria-describedby="emailHelp"
              value={this.state.customer.email}
              onChange={this.handleChangeEmail}
            />
            <div className="invalid-feedback-def">{this.state.emailError}</div>
            <div id="emailHelp" className="form-text">
              We'll never share your email with anyone else.
            </div>
          </div>
          <div className="invalid-feedback-def mb-2">
            {this.state.requestError}
          </div>
          <button
            className="btn btn-primary"
            disabled={this.state.isError}
            onClick={this.handleSubmit}
          >
            {!this.state.isUpdated && "Register"}
            {this.state.isUpdated && "Update"}
          </button>
        </form>
      </div>
    );
  }
}

export default Register;
