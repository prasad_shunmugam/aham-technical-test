import React from "react";
import { Link } from "react-router-dom";

class Customer extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            customer: {
                id: 'loading...',
                firstName: "loading...",
                lastName: "loading...",
                email: "loading..."
            },
            isDeleted: false
        };

        this.handleClick = this.handleClick.bind(this);
    }  

    async componentDidMount() {
        const { id } = this.props.match.params;
        const url = `http://localhost:4000/api/customers/${id}`;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({customer: data});
    }

    async handleClick(event) {
        const id = this.state.customer.id
        const url = `http://localhost:4000/api/customers/${id}`;

        fetch(url, {method: 'DELETE'}).then((response) => {
            return response.json();
        }).then((data) => {
            this.setState({isDeleted: true});
            console.log('Deleted user: ',data)            
        });
    }

    render() {
        return (
        <div className="container">
            <h1>Customer Profile</h1>
            <div className="card text-center">
                <div className="card-header">
                    Customer Profile
                </div>
                <div className="card-body">
                    <h5 className="card-title">Fullname: {this.state.customer.firstName} {this.state.customer.lastName}</h5>
                    <p className="card-text">Email: {this.state.customer.email}</p>
                    <Link to='/existing_customer'>
                        <button type="button" className="btn btn-danger" onClick={this.handleClick}>
                            Delete
                        </button> 
                    </Link>
                    <Link to={`/register_customer/${this.state.customer.id}`}>
                        <button type="button" className="btn btn-success ml-3">
                            Update
                        </button>                       
                    </Link>                        
                </div>
                <div className="card-footer text-muted">
                    customer ID: {this.state.customer.id}
                </div>
            </div>
        </div>
        );
    }
}

export default Customer;
