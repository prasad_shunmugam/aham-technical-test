import './App.css';
import Home from './components/Home';
import Register from './components/Register';
import Existing from './components/Existing';
import Customer from './components/Customer';
import Nav from './components/Nav'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Nav />

      <div >
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/register_customer' exact component={Register} />
          <Route path='/register_customer/:id' component={Register} />
          <Route path='/existing_customer' exact component={Existing} />
          <Route path='/customer_profile/:id' component={Customer} />
        </Switch>
      </div>

    </Router>     

  );
}

export default App;
