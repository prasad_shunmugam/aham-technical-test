# AHAM Technical Test

--------------------------------------------------------Description----------------------------------------------------------

Build a rest API endpoints using nodejs/python 

and

Build a small SPA using vuejs, react, angular which will be a client-side interface for customer information management.

-----------------------------------------------------------------------------------------------------------------------------



--------------------------------------------------------Start APP----------------------------------------------------------

First you'll need to git clone this repo. Once thats done you'll need to open 2 terminals (command prompts)

Terminal 1
1. cd to "aham-technical-test/aham-client-side"
2. run this command "npm install", this will install the required packages
3. run this command "npm start", this will startup the react client side

3. 
Terminal 2
1. cd to "aham-technical-test/aham-server-side", please note the file name is "server"
2. run this command "npm install", this will install the required packages 
3. run this command "node index.js", this will startup the Node API server 


Once these two terminals are running, you will be able to use the app and register customers for AHAM.

Thank you!

-----------------------------------------------------------------------------------------------------------------------------
